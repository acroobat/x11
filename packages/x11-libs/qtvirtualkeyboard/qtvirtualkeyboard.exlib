# Copyright 2016-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure

SUMMARY="Qt Cross-platform application framework: Qt Virtual Keyboard"
DESCRIPTION="
Qt Virtual Keyboard is a virtual keyboard framework that consists of a C++
backend supporting custom input methods as well as a UI frontend implemented
in QML."

LICENCES="
    GPL-3
    pinyin? ( Apache-2.0 )
"
MYOPTIONS="
    examples
    hangul [[ description = [ Enables the Hangul input method for the Korean language ] ]]
    pinyin [[ description = [ Enables the Pinyin input method for Simplified Chinese ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-spell/hunspell:=
        x11-libs/libxcb
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
        x11-libs/qtsvg:${SLOT}[>=${PV}]
        examples? ( x11-libs/qtquickcontrols2:${SLOT}[>=${PV}] )
        hangul? ( cjk/libhangul )
        pinyin? ( inputmethods/libpinyin )
"
# TODO: Other languages and input methods
# myscript - "World’s best handwriting recognition for text, math, graphics and music"
# openwnn -  OpenWnn input method for the Japanese language
# tcime - Cangjie and Zhuyin input methods for the Traditional Chinese language
# t9write - Handwriting recognition based on the commercial T9 Write product
# lipi-toolkit - Handwriting recognition based on the open source Lipi Toolkit

qtvirtualkeyboard_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi
    option hangul && EQMAKE_PARAMS+=( CONFIG+=hangul )
    option pinyin && EQMAKE_PARAMS+=( CONFIG+=pinyin )

    qmake_src_configure
}

